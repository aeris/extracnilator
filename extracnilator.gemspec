Gem::Specification.new do |s|
	s.name = 'extracnilator'
	s.version = '0.1.0'
	s.licenses = ['AGPL-3.0-or-later']
	s.summary = 'Scrapping du site de la CNIL pour en extraire les plaintes réalisées'
	s.authors = ['aeris']
	s.email = ['aeris@imirhil.fr']
	s.homepage = 'https://git.imirhil.fr/aeris/extracnilator/'
	s.metadata = { 'source_code_uri' => 'https://git.imirhil.fr/aeris/extracnilator/src/branch/ruby' }

	s.files = `git ls-files -z`.split("\x0")
	s.executables = s.files.grep(%r{^bin/}) { |f| File.basename(f) }
	s.test_files = s.files.grep(%r{^(test|spec|features)/})

	s.add_dependency 'http-cookie', '~> 1.0.7'
	s.add_dependency 'nokogiri', '~> 1.16.7'
	s.add_dependency 'playwright-ruby-client', '~> 1.47.0'
	s.add_dependency 'amazing_print', '~> 1.6'
	s.add_dependency 'faraday-cookie_jar', '~> 0.0.7'
	s.add_dependency 'faraday-follow_redirects', '~> 0.3.0'
	s.add_dependency 'parslet', '~> 2.0.0'

	s.add_development_dependency 'pry', '~> 0.14.2'
	s.add_development_dependency 'pry-byebug', '~> 3.10.1'
	s.add_development_dependency 'rubocop', '~> 1.66.1'
end
