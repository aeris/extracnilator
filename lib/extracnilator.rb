# frozen_string_literal: true

require 'logger'
require 'nokogiri'
require 'date'

class Extracnilator
	def self.process(**kwargs)
		self.new(**kwargs).process
	end

	def process
		self.login
		browser = if @screenshot
			          -> (&block) { Browser.new @jar, &block }
			      else
				      -> (&block) { block.call }
		          end
		browser.call do |playwright|
			@logger.info { 'Pending cases' }
			response = self.pending_cases
			cases = response.css 'ul li'
			# cases = cases[..0]
			cases.each { self.scrap playwright, _1 }
			@logger.info { 'Closed cases' }
			response = self.closed_cases
			cases = response.css 'ul li'
			# cases = []
			cases.each { self.scrap playwright, _1 }
		end
	end

	private

	LOGGER = Logger.new $stdout, level: Logger::INFO, formatter: -> (*args) { args.last + "\n" }
	FARADAY_OPTIONS = {
			url: 'https://services.cnil.fr',
			# proxy: 'http://localhost:8080',
			# ssl: {
			# 		ca_file: '/home/aeris/.mitmproxy/mitmproxy-ca-cert.cer'
			# }
	}

	def initialize(login: nil, directory: nil, anonymize: false, screenshot: true, logger: nil)
		@login = login
		@directory = directory || Dir.pwd
		@anonymize = anonymize
		@screenshot = screenshot
		@logger = logger
		@jar = LWPCookieJar.new File.join(@directory, 'cookies.dat')
		@client = Client.new @jar, FARADAY_OPTIONS
	end

	def login
		return @jar.load unless @login

		@logger.info { 'Log in...' }
		username, password = @login
		response = @client.get '/login/', { next: '/' }
		url = response.env.url.to_s

		response = Nokogiri::HTML response.body
		csrf = response.at('#login-password-form input[name=csrfmiddlewaretoken]')[:value]

		@client.post url do |req|
			req.headers['Content-Type'] = 'application/x-www-form-urlencoded'
			req.headers['Referer'] = url
			req.body = URI.encode_www_form(
					csrfmiddlewaretoken: csrf,
					username: username,
					password: password,
					'login-password-submit': ''
			)
		end

		@jar.save
		@logger.info { 'Logged!' }
	end

	RE_DATE = Regexp.compile(%r{Le formulaire a été enregistré le (\d{2}/\d{2}/\d{4} \d{2}:\d{2}) avec le numéro .*\.}).freeze

	def pending_cases
		response = @client.get '/ajax/cell/16/wcs_wcscurrentformscell-12/'
		Nokogiri::HTML response.body
	end

	def closed_cases
		response = @client.get '/ajax/cell/16/wcs_wcscurrentformscell-13/'
		Nokogiri::HTML response.body
	end

	NODES_TO_ANONYMIZE = [
			'span.connected-user--first-name', # User name
			'span.connected-user--last-name', # User lastname
			'div.field.username > span.value', # User name
			'#form-field-label-f60 + div.value', # Name
			'#form-field-label-f61 + div.value', # Lastname
			'#form-field-label-f67 + div.value', # Birth name
			'#form-field-label-f68 + div.value', # Address
			'#form-field-label-f69 + div.value', # City
			'#form-field-label-f71 + div.value', # Email
			'#form-field-label-f229 + div.value', # Phone
	]

	def scrap(playwright, complaint)
		link = complaint.at 'a'
		return if link.at('span.form-status').text == 'Brouillon'

		base_url = link['href']

		number = link.at('span.form-number')
		controller = number.at('span.form-digest')&.text[1..-2]
		number = number.children.first.text.strip
		@logger.info { "Processing #{number}" }
		@logger.info { "#{base_url} #{number} #{controller}" }
		directory = File.join @directory, "#{number}_#{controller}"
		FileUtils.mkdir_p directory
		response = @client.get base_url
		response = response.body
		response = Nokogiri::HTML response
		response.at('div.section.foldable.folded').remove_class 'folded'
		NODES_TO_ANONYMIZE.each do
			response.at(_1)&.content = '<anonymized>'
		end if @anonymize
		case_html = File.join directory, "#{number}.html"
		File.binwrite case_html, response

		complaint = response.at('.field-type-text > div:nth-child(2)').text

		items = response.css('ul#evolutions > li').collect do |item|
			if (status = item.at 'span.status')
				status = status.text.strip
			end
			if (date = item.at 'span.time')
				date = date.text.strip
				date = DateTime.strptime date, '%d/%m/%Y %H:%M'
			end
			if (msg = item.at 'div.msg')
				msg = msg.text.strip
			end

			attachments = item.css('p.wf-attachment > a').collect do |attachment|
				name = attachment.text
				@logger.info { "  Downloading %#{name}" }
				href = attachment['href']
				href = URI.join base_url, href
				content = @client.get(href).body
				File.binwrite File.join(directory, name), content
				name
			end

			item = { date:, status:, msg: }
			item[:attachments] = attachments unless attachments.empty?
			item
		end

		attachments = response.css('.field-type-file .file-field a').collect do |attachment|
			href = attachment['href']
			href = URI.join base_url, href
			name = attachment.at('span').text
			@logger.info { "  Downloading #{name}" }
			content = @client.get href
			File.binwrite File.join(directory, name), content.body
			name
		end

		date = response.at 'div.text-form-recorded'
		if date
			date = RE_DATE.match(date.text)[1]
			date = DateTime.strptime date, '%d/%m/%Y %H:%M'
		end

		content = {
				date: date,
				controller: controller,
				case: complaint,
				attachments: attachments,
				items: items
		}

		case_yaml = File.join directory, "#{number}.yaml"
		File.open case_yaml, 'w' do |io|
			YAML.dump content, io
		end

		if @screenshot
			@logger.info { "  Screenshotting" }
			playwright.goto base_url do |page|
				page.click 'span.disclose-message'
				page.wait_for_selector 'div.field.username'
				case_png = File.join directory, "#{number}.png"
				page.screenshot path: case_png, fullPage: true
			end
		end
	end
end

require_relative 'extracnilator/cookie_jar'
require_relative 'extracnilator/client'
require_relative 'extracnilator/playwright'
