# frozen_string_literal: true

require 'faraday'
require 'faraday/cookie_jar'
require 'nokogiri'

class Extracnilator
	class Client
		def initialize(jar, options = {})
			@client = Faraday.new(**options) do |builder|
				builder.use :cookie_jar, jar: jar
				builder.use Faraday::Response::RaiseError
			end
		end

		def get(*args, follow: true, **kwargs, &block)
			response = @client.get *args, **kwargs, &block
			response = self.follow response if follow
			response
		end

		def post(*args, follow: true, **kwargs, &block)
			response = @client.post *args, **kwargs, &block
			response = self.follow response if follow
			response
		end

		private

		def follow(response)
			while [302, 303].include? response.status
				origin = response.env.url
				location = response['location']
				response = get origin + location
			end
			response
		end
	end
end
