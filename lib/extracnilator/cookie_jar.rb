# frozen_string_literal: true

require 'http-cookie'
require 'yaml'
require 'parslet'

class Extracnilator
	class HashCookieJar
		def initialize(file)
			@cookies = Hash.new { _1[_2] = {} }
			@file = file
		end

		def load
			cookies = YAML.safe_load_file @file, permitted_classes: [HTTP::Cookie, Time], aliases: true
			@cookies.merge! cookies
		end

		def save
			File.open @file, 'w' do |io|
				YAML.dump @cookies, io
			end
		end

		def cookies(url)
			domain = url.host
			self[domain].collect { "#{_1}=#{_2.value}" }
		end

		def parse(set_cookie, url)
			HTTP::Cookie.parse(set_cookie, url).each { ap _1; self << _1 }
		end

		private

		ATTRIBUTES = %w[
      Domain Expires HttpOnly Max-Age
      Partitioned Path Secure SameSite
    ].freeze

		def [](domain)
			@cookies[domain]
		end

		def <<(cookie)
			domain = cookie.domain
			cookies = @cookies[domain]
			name = cookie.name
			value = cookie.value
			if value.empty? or cookie.expired?
				cookies.delete name
			else
				cookies[name] = cookie
			end
		end
	end

	class LWPCookieJar
		def initialize(file = 'cookies.dat')
			@file = file
			@cookies = []
		end

		def load
			File.open(@file).each do |line|
				next unless line.start_with? PREFIX
				line = line.delete_prefix(PREFIX).chomp
				line = PARSER.parse line
				cookie = TRANSFORM.apply line
				cookie, value = cookie.partition { |k, _| LWP_ATTRIBUTES.include? k }
				raise "Multiple names cookie: #{value}" unless value.size == 1
				cookie = cookie.to_h
				cookie[:httponly] = true unless cookie[:HttpOnly] == 'None'
				cookie[:secure] = true if cookie.has_key? :secure
				cookie[:session] = true if cookie.has_key? :discard
				name, value = value.first
				self << HTTP::Cookie.new(name: name.to_s, value: value, **cookie)
			end
			true
		end

		def save
			File.open @file, 'w' do |io|
				io.puts '#LWP-Cookies-2.0'
				@cookies.each do |cookie|
					io.write PREFIX

					output = [
							[cookie.name, '"' + cookie.value + '"'],
							[:path, '"' + cookie.path + '"'],
							[:domain, '"' + cookie.domain + '"'],
							[:version, 0],
							[:HttpOnly, cookie.httponly ? nil : 'None'],
							[:expires, cookie.expires&.strftime('"%F %TZ"')]
					].reject { |_, v| v.nil? }
					 .collect { "#{_1}=#{_2}" }
					output << :secure if cookie.secure
					output << :discard if cookie.session

					io.puts output.join '; '
				end
			end
		end

		def cookies(url)
			domain = url.host
			@cookies.select { _1.domain == domain }
		end

		def parse(set_cookie, url)
			HTTP::Cookie.parse(set_cookie, url).each { self << _1 }
		end

		def [](name)
			@cookies.find { _1.name == name }
		end

		def <<(cookie)
			@cookies.delete_if { _1.name == cookie.name }
			@cookies << cookie
		end

		private

		PREFIX = 'Set-Cookie3: '.freeze
		LWP_ATTRIBUTES = %i[
      path domain expires HttpOnly SameSite version path_spec secure discard
    ].freeze

		class LWPCookieParser < Parslet::Parser
			separators = "()<>@,;:\\\"/[]?={} \t".each_char.to_a.freeze
			SEPARATORS = Regexp.escape(separators.join).freeze
			char = (' '..'~').to_a.freeze
			CHAR = Regexp.escape(char.join).freeze
			QDTEXT = Regexp.escape((char - ['"']).join).freeze
			TOKEN = Regexp.escape((char - separators).join).freeze

			rule(:space?) { str(' ').repeat }
			rule(:separator) { match[SEPARATORS] }
			rule(:qdtext) { match[QDTEXT] }
			rule(:token) { match[TOKEN].repeat(1) }
			rule(:attribute) { token }
			rule(:quoted_pair) { str('\\') >> match[CHAR] }
			rule(:quoted_string) { str('"') >> (qdtext | quoted_pair).repeat.as(:value) >> str('"') }
			rule(:parameter) { attribute.as(:name) >> str("=") >> value }
			rule(:value) { token.as(:value) | quoted_string }
			rule(:part) { parameter | token.as(:name) }
			rule(:parts) { part.as(:part) >> (str(";") >> space? >> part.as(:part)).repeat }
			rule(:line) { parts.as :parts }
			root(:line)
		end

		PARSER = LWPCookieParser.new

		class LWPCookieTransform < Parslet::Transform
			rule(:part => subtree(:part)) do
				[part[:name].to_sym, part[:value]&.to_s]
			end
			rule(:parts => subtree(:parts)) do
				parts.to_h
			end
		end

		TRANSFORM = LWPCookieTransform.new
	end
end
