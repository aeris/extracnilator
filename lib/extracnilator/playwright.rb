# frozen_string_literal: true
require 'playwright'

class Extracnilator
	class Browser
		BROWSER_CONFIG = {
				# headless: false
		}.freeze
		CONTEXT_CONFIG = {
				# "proxy": {"server": "http://localhost:8081"},
				# "ignore_https_errors": True
		}.freeze

		def initialize(jar)
			@jar = jar
			Playwright.create playwright_cli_executable_path: './node_modules/.bin/playwright' do |playwright|
				@browser = playwright.firefox.launch(**BROWSER_CONFIG)
				yield self
			end
		end

		def goto(url)
			cookies = @jar.cookies(URI(url)).collect do |cookie|
				{
						name: cookie.name,
						value: cookie.value,
						domain: cookie.domain,
						path: cookie.path,
						httpOnly: cookie.httponly,
						secure: cookie.secure,
						sameSite: 'None'
				}
			end
			@browser.new_context(**CONTEXT_CONFIG) do |context|
				context.add_cookies cookies
				page = context.new_page
				page.goto url
				yield page
			end
		end

		def screenshot_telerecours(url, img)
			cookies = @jar.cookies(URI(url)).collect do |cookie|
				{
						name: cookie.name,
						value: cookie.value,
						domain: cookie.domain,
						path: cookie.path,
						httpOnly: cookie.httponly,
						secure: cookie.secure,
						sameSite: 'None'
				}
			end
			@browser.new_context(**CONTEXT_CONFIG) do |context|
				context.add_cookies cookies
				page = context.new_page
				page.goto url
				page.click 'span.disclose-message'
				page.wait_for_selector 'div.field.username'
				page.screenshot path: img, fullPage: true
			end
		end
	end
end
