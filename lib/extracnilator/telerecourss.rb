class Extracnilator
	class TelerecouRSS
		def initialize(login)
			@login = login
			@jar = LWPCookieJar.new 'cookies.telerecours.dat'
			@client = Client.new @jar, FARADAY_OPTIONS
		end

		def process(directory = 'cases')
			self.login
			response = @client.get '/api/requetes', follow: false
			response = JSON.parse response.body
			base_url = URI FARADAY_OPTIONS[:url]
			Browser.new @jar do |playwright|
				response.each do |request|
					id, number, title, status, date = request.values_at 'id', 'numeroRequete', 'titre', 'statut', 'dateModification'
					date = DateTime.parse(date).to_date
					LOGGER.info { "Processing #{number} #{title}" }
					LOGGER.info { "  #{date} #{status}" }
					response = @client.get("/api/requetes/#{id}").body
					File.write File.join(directory, "#{id}.json"), response
					url = base_url.dup
					url.fragment = "/historique/#{id}"
					png = File.join directory, "#{id}.png"
					playwright.goto url do |page|
						page.wait_for_selector 'jhi-requete-title'
						page.screenshot path: png, fullPage: true
					end
				end
			end
		end

		def self.process(login: false, directory: 'cases')
			self.new(login).process directory
		end

		protected

		FARADAY_OPTIONS = {
				url: 'https://citoyens.telerecours.fr'
		}

		def login
			return @jar.load unless @login
			username, password = @login
			@client.get '/api/authenticate'
			response = @client.post '/api/authentication', follow: false do |req|
				req.headers['Content-Type'] = 'application/x-www-form-urlencoded'
				req.headers['X-XSRF-TOKEN'] = @jar['XSRF-TOKEN'].value
				req.body = URI.encode_www_form(
						j_username: username,
						j_password: password,
						'remember-me': true,
				)
			end
			raise 'Unable to login' unless response.status == 200
			response = @client.get '/api/authenticate'
			raise 'Unable to login' unless response.body == username
			@jar.save
		end
	end
end
